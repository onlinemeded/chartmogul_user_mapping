# README #


### What is this repository for? ###

This is the accumulated scripts designed to merge customer data in Chart Mogul

## Sources of Truth for Account Connection
- Stripe Data 
    - Current: Exported from website, imported into local database
    - Planned/Desired: Retrieved via API and inserted into shared DB & regularly updated
- Braintree Data
    - Retrieved via API at the time the script ran 11/28/2018
    - Planned/Desired: Retrieved via API and inserted into shared DB & regularly updated
- Recurly Data
    - Retrieved via API at the time the script ran 11/28/2018
    - Planned/Desired: Retrieved via API and inserted into shared DB & regularly updated

## Continuing Issues
- Unified Customer - Not every user is a customer
    - Should every user be represented in Chart Mogul? 
    - Solution: Add a 4th data source for API, use API to make each user a CM customer based on user ID in our system
- Subscriptions & Churn
    - Should subscriptions of equal value be connected?
    - Should "upfront" subscriptions still count as subscriptions for CM's purposes? Right now they count as non subscription revenue.
- Keeping data unified on a continuous and automatic basis
    - Solution: Run a script at regular intervals to collect and update the shared transaction DB

## Account Connection Criteria

1. Integrations were completed using default settings in CM

2. Customers have been merged via the Chart Mogul API using the stored external IDs for Braintree and Recurly present on our in house database. This merge only occurs if the customer was imported into Chart Mogul in the first place (which only includes paying customers)

3. A script has been executed to connect a specific set of subscriptions. The criteria for these connections are:

    - A Braintree subscription that was cancelled via the API by Will between 5/1/2018 and 6/5/2018

    - A Recurly subscription that started between 5/1/2018 and 6/5/2018

    - The subscription plan text matches (i.e. “Special” or “Annual Up Front”)

4. Stripe transactions that could be associated with an existing CM user are associated with that account
    - Completed by using manually exported Stripe data in a local DB and getting the user info with a query
    - Stripe data was connected by email to existing Recurly customers in manually export Recurly data

## General Information Flow

In Chart Mogul a customer has to be merged FROM one record INTO another record. The order these were executed are as follows:

- FROM: Braintree INTO: Recurly
- FROM: Stripe INTO: Recurly

- Recurly was used as a defacto default user, but not every user has a Recurly user in CM so this may need adjusting

## Detailed Script Steps:
1. Establish External Connections:
    1. Chart Mogul API
    2. Braintree API
    3. Billing DB
    4. Local DB (Containing Export of 3 Providers Data as of 12/1/2018)
2. Merge Customers that exist in Braintree and Recurly 
    1. Determined by customers who have an external ID in our DB for both Recurly & Braintree (using _recurly_customers_ & _braintree_customers_ tables)
    2. Relevant functions: 
        1. _getAllBraintreeAndRecurlyAccounts_ 
        2. _mergeRecurlyAndBraintreeCustomers_
3. Connect Subscriptions that were interrupted by move to Recurly
    1. Determined by the criteria listed above
    2. All Chart Mogul customers gathered and returned as a list
    3. Customer list looped through and subscriptions for each customer retrieved via API
    4. Subscription data groomed
        1. Subscription tested for "danger" date range
        2. Subscription checked to see if it matches another subscription
    5. Customers looped through and subscription pairs are connected
4. Connect Stripe Transactions
    1. A local transaction DB was created by exporting all data from all 3 services: Braintree, Recurly, Stripe
    2. This data was queried and Stripe transactions were checked for connection eligibility (using the email associated with the order)
    3. All eligible Stripe transactions were connected to the appropriate CM account
