#############################
# IMPORTS
#############################
# Chart Mogul Imports
import chartmogul

#  MySQL Connector Imports
import mysql.connector

#  Python Requests import
import requests
from requests.auth import HTTPBasicAuth

# JSON import for decoding JSON data
import json

# Dotenv imports
import os
from dotenv import load_dotenv
load_dotenv()

# Pretty Print
import pprint

# Datetime
from datetime import datetime, timedelta, date

# Braintree Imports
import braintree

# Recurly Imports
import recurly
recurly.SUBDOMAIN = "onlinemeded"
recurly.API_KEY = os.getenv("RECURLY_PRIVATE_KEY")

# Asyncio Imports
import asyncio
import concurrent.futures

# Pandas import
import pandas as pd

# Numpy import
import numpy as np

#time
import time

#Stripe
import stripe

import functools

import zipcodes



########################
# CONFIGURATION
########################
# Chart Mogul Configuration (uses access tokens from admin page)
chartMogulAccountToken = os.getenv("CHART_MOGUL_ACCOUNT_TOKEN")
chartMogulSecretKey = os.getenv("CHART_MOGUL_SECRET_KEY")
config = chartmogul.Config(chartMogulAccountToken, chartMogulSecretKey)

# Stripe
stripe.api_key = os.getenv("STRIPE_API_KEY")

# Method to confirm Chart Mogul Connection
def confirmChartMogulConnection():
    print(chartmogul.Ping.ping(config).get())
    # print(chartmogul.DataSource.all(config))

# Braintree Connection
gateway = braintree.BraintreeGateway(
    braintree.Configuration(
        braintree.Environment.Production,
        merchant_id=os.getenv("BRAINTREE_MERCHANT_ID"),
        public_key=os.getenv("BRAINTREE_PUBLIC_KEY"),
        private_key=os.getenv("BRAINTREE_PRIVATE_KEY")
    )
)

# Connecting to the Billing Database
mybillingdb = mysql.connector.connect(
  host=os.getenv("BILLING_DB_HOST"),
  user=os.getenv("BILLING_DB_USER"),
  passwd=os.getenv("BILLING_DB_PW"),
  database="billing"
)

# Connecting to the Prod Database
myproddb = mysql.connector.connect(
    host=os.getenv("PROD_DB_HOST"),
    user=os.getenv("PROD_DB_USER"),
    passwd=os.getenv("PROD_DB_PW"),
    database = 'production')

# # Connecting to the Local DB. This is the database that contains the error table for tracking errors the script encounters during the historic import. 
# mylocaldb = mysql.connector.connect(
#     host='127.0.0.1',
#     user='root',
#     passwd='',
#     database = 'external_services'
# )

# Creates the object that will execute queries
mybillingcursor = mybillingdb.cursor()
myprodcursor=myproddb.cursor()
# mylocalcursor=mylocaldb.cursor()

# Chart Mogul API IDs
API_ID =   'ds_c525ad12-2018-11e9-8f36-5b1d2d07054e'# This is the API ID for the data source from ChartMogul

##################################
# HELPER FUNCTIONS
##################################
# Accepts a plan code and returns the corresponding chart mogul plan uuid 
def returnPlanUUID(id):
    # This array of plans is just the pasted response from the https://api.chartmogul.com/v1/plans endpoint. 
    plans= [
         {
            "external_id": "two-years-up-front",
            "name": "Two Years Up Front",
            "interval_count": 12,
            "uuid": "pl_f7f1b7a4-2018-11e9-9189-83bd5b7b7cc2",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "free-premium-basic-sciences",
            "name": "Free Premium Basic Sciences",
            "interval_count": 1,
            "uuid": "pl_f58651be-2018-11e9-9189-d734615f4bb2",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "annual-365-promo",
            "name": "July 365 promo",
            "interval_count": 12,
            "uuid": "pl_f51a2034-2018-11e9-b8d6-b38bfef9b2e7",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly-biochem-cell-basic-sciences",
            "name": "Basic Sciences Biochem Cell Monthly",
            "interval_count": 1,
            "uuid": "pl_f497d0ac-2018-11e9-b8d6-e762e020962a",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "full-price-monthly-clinical",
            "name": "Clinical Monthly",
            "interval_count": 1,
            "uuid": "pl_f426fc2e-2018-11e9-b8d6-9fd6a0d02c94",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "free-monthly-clinical",
            "name": "Clinical Free Monthly",
            "interval_count": 1,
            "uuid": "pl_f3b75fae-2018-11e9-b8d6-f3a7abb071b2",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly-discount-clinical-clinical",
            "name": "Clinical Discount Monthly",
            "interval_count": 1,
            "uuid": "pl_f34e07d4-2018-11e9-b8d6-2b84370f2d7c",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "six-months-up-front-clinical",
            "name": "Clinical 6 Month",
            "interval_count": 6,
            "uuid": "pl_f2cf4746-2018-11e9-b8d6-3b0a9c862732",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "three-months-up-front-clinical",
            "name": "Clinical 3 Month",
            "interval_count": 3,
            "uuid": "pl_f254d1f0-2018-11e9-b8d6-a34795634ea0",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "annual-up-front-basic-sciences",
            "name": "Basic Sciences 12 Month",
            "interval_count": 12,
            "uuid": "pl_f1d0296e-2018-11e9-9189-9f5fc9f61748",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "2yr",
            "name": "2 Years Up Front",
            "interval_count": 24,
            "uuid": "pl_f0bbd3c0-2018-11e9-9189-27566ca690c6",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "10mo",
            "name": "10 Months Up Front",
            "interval_count": 10,
            "uuid": "pl_f04f4a34-2018-11e9-b8d6-03041114a723",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "special-recurring",
            "name": "Special Recurring",
            "interval_count": 1,
            "uuid": "pl_ef5499a4-2018-11e9-9189-b771e605fef3",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "three-months-up-front",
            "name": "Three Months Up Front",
            "interval_count": 3,
            "uuid": "pl_ed60f610-2018-11e9-9189-ff6d659642bb",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "ann1",
            "name": "Ann1 (Deleted)",
            "interval_count": 12,
            "uuid": "pl_ece320dc-2018-11e9-9189-2f222d3ceccf",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "annual",
            "name": "Annual (Deleted)",
            "interval_count": 1,
            "uuid": "pl_ec672554-2018-11e9-9189-23fa08b56d1d",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "sixmonth",
            "name": "Sixmonth (Deleted)",
            "interval_count": 6,
            "uuid": "pl_ebe9287a-2018-11e9-b8d6-9795b84fe5d5",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "ann2",
            "name": "Ann2 (Deleted)",
            "interval_count": 1,
            "uuid": "pl_eb6c4f76-2018-11e9-b8d6-235abd8c2920",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "5mo",
            "name": "5mo (Deleted)",
            "interval_count": 5,
            "uuid": "pl_eafca37e-2018-11e9-9189-7357f6b28adf",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "4mo",
            "name": "4mo (Deleted)",
            "interval_count": 4,
            "uuid": "pl_ea82e52a-2018-11e9-9189-4fddcbc8a50a",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "eightmonth",
            "name": "Eightmonth (Deleted)",
            "interval_count": 8,
            "uuid": "pl_ea08167e-2018-11e9-9189-5f33223b7a5b",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "aa1",
            "name": "Aa1 (Deleted)",
            "interval_count": 1,
            "uuid": "pl_e98c61a0-2018-11e9-b8d6-33ad57fb9d48",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "tyler",
            "name": "Tyler (Deleted)",
            "interval_count": 12,
            "uuid": "pl_e90c384a-2018-11e9-b8d6-937429562e97",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "test3",
            "name": "Test3 (Deleted)",
            "interval_count": 1,
            "uuid": "pl_e88db5f6-2018-11e9-9189-5bc2e85d83fd",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "testtrialplan",
            "name": "Testtrialplan (Deleted)",
            "interval_count": 1,
            "uuid": "pl_e7f9375a-2018-11e9-9189-23675b089b4a",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "3mo",
            "name": "3 month (Deleted)",
            "interval_count": 3,
            "uuid": "pl_e7839036-2018-11e9-b8d6-1fde4ecf1874",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "full-price-monthly",
            "name": "Full price monthly",
            "interval_count": 1,
            "uuid": "pl_e7083936-2018-11e9-b8d6-1f421e2d8fc3",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "free-monthly",
            "name": "Free Monthly",
            "interval_count": 1,
            "uuid": "pl_e68f2b22-2018-11e9-a088-33fda580d69e",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "eight-months-up-front",
            "name": "Eight Months Up Front",
            "interval_count": 8,
            "uuid": "pl_e615ee24-2018-11e9-9189-ebdecf493b6c",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "discount-annual-billed-monthly",
            "name": "Discount annual billed monthly",
            "interval_count": 1,
            "uuid": "pl_e59f96ca-2018-11e9-9189-5b9ff6e16d4e",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "annual-up-front",
            "name": "Annual Paid Up Front",
            "interval_count": 12,
            "uuid": "pl_e524c936-2018-11e9-9189-d367003a5f44",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "legacy-annual-billed-monthly",
            "name": "Legacy Annual Billed Monthy",
            "interval_count": 1,
            "uuid": "pl_e4ab2662-2018-11e9-a088-ff9aad0af494",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "nine-months-up-front",
            "name": "Nine Months Up Front",
            "interval_count": 8,
            "uuid": "pl_e43a6788-2018-11e9-8a23-0b2e8d129c89",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "six-months-up-front",
            "name": "Six Months Up Front",
            "interval_count": 6,
            "uuid": "pl_e3bb02b8-2018-11e9-8a23-234ad9fedbf1",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "special",
            "name": "Special",
            "interval_count": 1,
            "uuid": "pl_e33d9026-2018-11e9-8a23-43bbd816b70c",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "eight-months-up-front-clinical",
            "name": "Clinical 8 Month",
            "interval_count": 8,
            "uuid": "pl_e2c2297c-2018-11e9-a088-53d53878f609",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly-discount-basic-sciences-basic-sciences",
            "name": "Basic Sciences Discount Monthly",
            "interval_count": 1,
            "uuid": "pl_e2536fd2-2018-11e9-8a23-1399c526c52a",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "discount-annual-billed-monthly-basic-sciences",
            "name": "Basic Sciences Annual",
            "interval_count": 1,
            "uuid": "pl_e1e3b390-2018-11e9-8a23-87f43b391e87",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly",
            "name": "Monthly (Deleted)",
            "interval_count": 1,
            "uuid": "pl_e17a6e12-2018-11e9-8a23-2b587d09cb36",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "nine-months-up-front-clinical",
            "name": "Clinical 9 Month",
            "interval_count": 9,
            "uuid": "pl_e0ed2534-2018-11e9-8a23-df972ba7dac2",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "three-months-up-front-basic-sciences",
            "name": "Basic Sciences 3 Month",
            "interval_count": 3,
            "uuid": "pl_e076cf56-2018-11e9-8a23-2f15df15862d",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "six-months-up-front-basic-sciences",
            "name": "Basic Sciences 6 Month",
            "interval_count": 6,
            "uuid": "pl_e004d3f6-2018-11e9-8a23-bbfb50e4fb27",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "eight-months-up-front-basic-sciences",
            "name": "Basic Sciences 8 Month",
            "interval_count": 8,
            "uuid": "pl_df868cb2-2018-11e9-8a23-f3fc1c2699e7",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly-biochem-basic-sciences",
            "name": "Basic Sciences Biochem Monthly",
            "interval_count": 1,
            "uuid": "pl_df0df518-2018-11e9-8a23-d7e1db36f683",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "free-premium-clinical",
            "name": "Free Premium Clinical",
            "interval_count": 1,
            "uuid": "pl_de9c9f1c-2018-11e9-8a23-03431ab2dbd4",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "ten-months-up-front",
            "name": "Ten Months Up Front",
            "interval_count": 10,
            "uuid": "pl_de2394a0-2018-11e9-b2c9-4bdaea77bd4c",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "2mo",
            "name": "2mo (Deleted)",
            "interval_count": 2,
            "uuid": "pl_dd72a8ca-2018-11e9-9189-db3a53ef43ee",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "9mo",
            "name": "9mo (Deleted)",
            "interval_count": 9,
            "uuid": "pl_dcfd7ab4-2018-11e9-a088-3ff3ca2e7290",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "monthly-biochem-cell-immune-basic-sciences",
            "name": "Basic Sciences Biochem Cell Immune Monthly",
            "interval_count": 1,
            "uuid": "pl_dc88f590-2018-11e9-8a23-9fc2197d0a02",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "full-price-monthly-basic-sciences",
            "name": "Basic Sciences Monthly",
            "interval_count": 1,
            "uuid": "pl_dc0de67a-2018-11e9-a343-8be4fe2a1941",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "annual-up-front-clinical",
            "name": "Clinical 12 Month",
            "interval_count": 12,
            "uuid": "pl_db9c772e-2018-11e9-8441-f7f6e7f10ec9",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "discount-annual-billed-monthly-clinical",
            "name": "Clinical Annual",
            "interval_count": 1,
            "uuid": "pl_db211d22-2018-11e9-9189-dbfcb3b2ea21",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        },
        {
            "external_id": "free-monthly-basic-sciences-basic-sciences",
            "name": "Basic Sciences Free Monthly",
            "interval_count": 1,
            "uuid": "pl_daaa5d86-2018-11e9-9189-33746a228bd5",
            "interval_unit": "month",
            "data_source_uuid": "ds_c525ad12-2018-11e9-8f36-5b1d2d07054e"
        }
    ]
    
    for plan in plans:
       if(plan['external_id']==id):
           return plan["uuid"]

#  Takes the invoice line item type and returns whether the purchase is a subscription or a one_time purchase
def returnIsSubscription(item_type):
        subscriptions = ["plan"]
        one_time = ["purchase", "credit", "debit", "carryforward"]     
        if(item_type in subscriptions):
            return "subscription"   
        else:
            return "one_time"

# A function that opens and reads the test email file
def listTesterEmails():
    text_file=open("exclude_emails.txt", "r")
    lines = text_file.read().split("\n")
    print(lines)

# Returns the Chart Mogul uuid of a given email, if that email was not the one used in CM, the function searches the customer table email
def getCMuuid(email):
    def alternate_email():
        try:
            user = usersOnID.loc[usersOnID['customer_email'] == email]
            alt_email = user['email']
            print("Alternate email: " + str(alt_email))
            customer = chartmogul.Customer.search(config, email=alt_email).get().entries[0]
            print(customer.uuid)
            return customer.uuid
        except Exception as ex:
            print('No Such Email: ' + str(email) )
            # appendErrors("getCMuuid", str(email))
            return None
        
    try:

        customer = chartmogul.Customer.search(config, email=email).get().entries[0]
        return customer.uuid
    except:
        print("Trying alternate email for: " + str(email))
        alternate_email()

# Creates a placeholder user for data that the appropriate user cannot be identified for. This user can later be merged into the correct user (if there is one). Most of these placeholders will be customers that are Stripe customers, but not subscribers. 
def createPlaceholderUser(user, service):
    try:
        if(service == "Stripe"):
            data = {
                "data_source_uuid": API_ID,
                "external_id": user['id'],
                "name": user['email'],
                "email": user['email'],
                "lead_created_at": user['created'],
                "free_trial_started_at": user['created']                    
            }
        elif(service == "Braintree"):
            data = {
                "data_source_uuid": API_ID,
                "external_id": "placeholder" + str(user.customer_details.id),
                "name": user.customer_details.email,
                "email": user.customer_details.email,
                "lead_created_at": user.created_at,
                "free_trial_started_at": user.created_at  
            }
        elif(service == "Recurly"):
            account = recurlyRateLimitWrapper(user.account())
            if(len(account.account_code) < 8): #If the account code is less than 8 digits, the recurly account code matches the user ID in our database
                ext_id = account.account_code
            else: #If it's longer, it's the temporary account code generated by recurly
                ext_id = "placeholder" + str(account.account_code)
            data = {
                "data_source_uuid": API_ID,
                "external_id": ext_id,
                "name": account.email,
                "email": account.email,
                "lead_created_at": account.created_at,
                "free_trial_started_at": account.created_at                    
            }
            
        customer = chartmogul.Customer.create(config, data=data).get()
        return customer.uuid
    except Exception as ex:
        appendErrors("createPlaceholderUser", "None", "None", str(data), "None", "None", str(ex))
        return None

# Returns true if the current recurly remaining calls number is below the desired threshold. Also has a kill switch in the event that the number dips too low. 
def underRateLimit():
    print(int(recurly.cached_rate_limits['remaining']) > 4000)
    if(recurly.cached_rate_limits['remaining'] <1500): # Kill switch if Recurly requests drop too low
        print("****************************\nKILLING SCRIPT, RECURLY OVERLOADED\n*****************************")
        quit()
    return int(recurly.cached_rate_limits['remaining']) > 4000
   
# A rate limit wrapper that only returns a function if the rate limit is above a safe threshold. Otherwise it waits and retries. 
def recurlyRateLimitWrapper(function):
    while(True):
        print(int(recurly.cached_rate_limits['remaining']))
        if(underRateLimit()):
            return function
        else:
            time.sleep(120)
            account = recurly.Account.get(12345) #This populates the rate limit value after the wait period (otherwise the script can get stuck thinking its over the rate limit forever)
            print("reattempting")
            return recurlyRateLimitWrapper(function)
                   
# Takes an array of items and calls a function on each of those items. Takes an optional number of workers argument to run the functions simultaneously on the given number of workers.
def runConcurrently(items, function, workers=10):
    async def main():

        with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:

            loop = asyncio.get_event_loop()
            futures=[
                loop.run_in_executor(
                    executor,
                    function,
                    item

                )for item in items # Iterates through the items and calls the function with each item
            ]

    loop=asyncio.get_event_loop()
    loop.run_until_complete(main())

# Writes errors and associated data to the local db for identifying edge cases or missed data, not enabled for deployed script
# # def writeErrors():    
#     global errors
#     errorCopy = errors
    
#     for i in range(0, len(errors), 500):
#         sql = """insert into errors(method,email, user_ID,payload,invoice_id,external_id,error,input,cm_uuid) values (%s,%s,%s,%s,%s,%s,%s,%s,%s);"""
    
#         db = mysql.connector.connect(
#         host='127.0.0.1',
#         user='root',
#         passwd='',
#         database = 'external_services'
#         )
#         cursor = db.cursor()
#         cursor.executemany(sql, errors[i:i+500])
#         db.commit()
#         print(cursor.rowcount)
#         db.close()
#     errors = []

# # Appends errors to the global error Array. Fills in 'None' for missing data    
# # def appendErrors(method = "None",email= "None", user_ID= "None",payload= "None",invoice_id= "None",external_id= "None",error= "None",input= "None",cm_uuid= "None"):
#     errors.append((str(method),str(email), str(user_ID),str(payload),str(invoice_id),str(external_id),str(error),str(input),str(cm_uuid)))

# A method to destroy a recurly invoice
def destroyRecurlyInvoice(invoice):
    print(invoice['external_id'])
    if(len(invoice['external_id']) < 8):
        print(invoice['external_id'], end='')
        print(chartmogul.Invoice.destroy(
                config,
                uuid=invoice['uuid']))

#############################
# INITIALIZE GLOBAL VARIABLES
#############################

recurly.Account.get(12345) #This populates the recurly rate limit value for the first time by retrieving an arbitrary account
errors = [] # Creates an empty global array for gathering errors

# Current Date/Time
now = datetime.now()
now_string = now.strftime("%Y-%m-%d")
now_timestamp = int(datetime.combine(now, datetime.max.time()).timestamp())

# Creates the start time as a subtracted interval of days
global_start = now - timedelta(days=6) # Script will run everyday, set to two days for overlap. Adding invoices more than once has no effect on the system, they just get kicked back.
global_start_string = global_start.strftime("%Y-%m-%d")
global_start_timestamp = int(datetime.combine(global_start, datetime.min.time()).timestamp())

# This function queries the production and billing databases and compiles user data and customer data joined by user ID. Returns a dataframe.
# Queries the production and billing databases and returns a unified dataframe that links user ID, recurly external ID, and braintree external ID.
def getUserData(start='2013-01-01', end=now_string):
    queryText = f"SELECT id, email, first_name, last_name, affiliate_code, created_at FROM users WHERE created_at between '{start}' and  '{end}' ;" 
    allUsers = pd.read_sql(queryText, myproddb)

    queryText = f"SELECT client_customer_id, email as customer_email, braintree_customers.ext_id as braintree, recurly_customers.ext_id as recurly, customers.created_at as customer_created_at FROM customers \
        LEFT JOIN braintree_customers on customers.id = braintree_customers.customer_id \
        LEFT JOIN recurly_customers on customers.id = recurly_customers.customer_id \
        WHERE customers.email NOT LIKE '%example.com%' \
        AND customers.email IS NOT NULL and customers.created_at between '{start}' and  '{end}' ;" 
    
    allCustomers = pd.read_sql(queryText, mybillingdb)
    
    allUsers = allUsers.astype(str)
    allCustomers = allCustomers.astype(str)
    
    usersAndCustomersOnID = allUsers.merge(allCustomers, left_on='id',right_on="client_customer_id", how="outer")
    usersAndCustomersOnEmail = allUsers.merge(allCustomers, left_on='email',right_on="customer_email", how="outer")

    print("Generated User Data")
    return [usersAndCustomersOnID, usersAndCustomersOnEmail]

# Retrieves and stores all users/customers merged either on user ID or email 
allUsers = getUserData() # Gathers all users

usersOnID = allUsers[0] #Users that are joined to the customer table on ID
usersOnEmail = allUsers[1] #Users that are joined to the customer table on email


usersOnID['created_at'] = pd.to_datetime(usersOnID['created_at']) # Casts the created_at column as a date
usersOnID['customer_created_at'] = pd.to_datetime(usersOnID['customer_created_at']) # Casts the customer_created_at column as date
newUsersOnID = usersOnID.loc[(usersOnID['created_at']>= global_start) | (usersOnID['customer_created_at']>= global_start)] # Creates new dataframe of users created after the provided start date

usersOnEmail['created_at'] = pd.to_datetime(usersOnEmail['created_at']) # Casts the created_at column as a date
usersOnEmail['customer_created_at'] = pd.to_datetime(usersOnEmail['customer_created_at']) # Casts the customer_created_at column as date
newUsersOnEmail = usersOnEmail.loc[(usersOnEmail['created_at']>= global_start) | (usersOnEmail['customer_created_at']>= global_start)] # Creates new dataframe of users created after the provided start date

#############################
# Create New Users
#############################

# Receives a user array, and sends that info to CM to create a user
def addUserToCM(user):
    
    try:   
        # This is the userID in our user table
        userID = user['id']
        if(userID == None):
            user = usersOnID.loc[usersOnID['email']==user['email']]
            userID = user['id']
        
        # Sometimes there are customers in Recurly that don't match their user table email
        if(user['email']==None): 
            email = user['customer_email']
        else: email = user['email']
         
        # Not all users have the names filled out, in the event that either name is missing, use the email instead    
        first_name = user['first_name']
        last_name = user['last_name']
        if(first_name == None or first_name == "" or last_name == None or last_name == "" or first_name=="nan" or last_name=="nan"):
            name = email
        else:
            name = str(first_name) + " " + str(last_name)
            
        affiliate_code=user['affiliate_code']
        recurly_id = user['recurly']
        braintree_id = user['braintree']
        created_at = user['created_at']
        
        # Creates an empty array for custom attributes and tags
        custom_attributes = []
        tags = []
        data = {}
        # Logical checks to see if the respective attributes are present
        if(affiliate_code != None):
            custom_attributes.append({"type": "String", "key": "Affiliate Code", "value": affiliate_code})
        if(recurly_id != None and not pd.isnull(recurly_id)):
            custom_attributes.append({"type": "String", "key": "Recurly", "value": recurly_id})
        if(braintree_id != None and not pd.isnull(braintree_id)):
            custom_attributes.append({"type": "String", "key": "Braintree", "value": braintree_id})

        # If custom attributes exist, executes the customer creation command with them added, otherwise they are left off
        if(pd.isnull(userID)):
            print("TEMP CUSTOMER") # This is a customer that is created in recurly but does not have a user account
            return
        if(len(custom_attributes)>0 and len(tags)>0):
            data={
                "data_source_uuid": API_ID,
                "external_id": int(userID),
                "name": name,
                "email": email,
                "lead_created_at": created_at,
                "free_trial_started_at": created_at,
                    "attributes": {
                            "custom": custom_attributes,                       
                            "tags": tags}
            }
            
        elif(len(custom_attributes)>0):
            data={
                "data_source_uuid": API_ID,
                "external_id": int(userID),
                "name": name,
                "email": email,
                "lead_created_at": created_at,
                "free_trial_started_at": created_at,
                    "attributes": {
                            "custom": custom_attributes
                        }
            }
            
        else:
            data = {
                "data_source_uuid": API_ID,
                "external_id": int(userID),
                "name": name,
                "email": email,
                "lead_created_at": created_at,
                "free_trial_started_at": created_at                    
            }
        chartmogul.Customer.create(
            config,
            data=data).get()
        print(userID)
       
    
    # Catches any failed API calls and writes to the appropriate file based on scenario (ignores pre-existing user errors)
    except Exception as ex:
        # appendErrors("addUserToCM", email, userID, str(data), None, None, str(ex), str(user),None)
        print(ex)        

# Runs addUsers concurrently. Note this function does not use the runConcurrently helper function because dataframes require an index when iterating over rows.
def runAddUsers(users=usersOnID):
    async def main():

        with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:

            loop = asyncio.get_event_loop()
            futures=[
                loop.run_in_executor(
                    executor,
                    addUserToCM,
                    item

                )for index, item in users.iterrows() # Iterates through the items and calls the function with each item
            ]

    loop=asyncio.get_event_loop()
    loop.run_until_complete(main())

#############################
# Add New Recurly Invoices
#############################
# A function that takes the users recurly account code and retrieves their customer information. This function returns an array of invoices groomed for the Chart Mogul API
def getRecurlyInvoicesByAPI(invoice):
    pprint.pprint(invoice)
    try:
        # A boolean variable that signifies if the invoice should be zeroed to prevent double charges in the system from the transition
        zeroInvoice = False 
        # Tests for dummy invoices. The invoices are zeroed instead of excluded because the line items are necessary for establishing a subscription
        # Invoices that were generated by Recurly in May 2018 during the transition from Braintree were classified as "manual" collections, but no money was actually collected
        # Chart mogul needs the subscription data from these invoices, but the fake/arbitrary monetary amounts were skewing the numbers
        # Bringing in these invoices will reflect the correct subscription in the API, but the subscription may not show up on the customers dashboard.
        # This appears to be because only subscriptions that generate revenue are reflected on the dashboard
        if(invoice.collection_method=='manual' and len(invoice.transactions)== 0): 
            zeroInvoice = True

        account = recurlyRateLimitWrapper(invoice.account())    
        account_code = account.account_code
        user =usersOnID.loc[usersOnID['recurly']== account_code] 
        email = user['email']
        cm_uuid = getCMuuid(email)
        if(cm_uuid==None):
            email = invoice.account().email
            cm_uuid = getCMuuid(email)
        if(cm_uuid == None):            
            cm_uuid = createPlaceholderUser(invoice, "Recurly")

        external_id = invoice.invoice_number
        date = invoice.created_at
        currency = invoice.currency

        # Creates the final invoice item template that will be populated with groomed data
        final_invoice = {
            "external_id": external_id,
            "date": date,
            "currency": currency,
            "line_items": [],
            "transactions": []
        }    

        # Every invoice in Chart Mogul needs a list of line items, this is how subscriptions are started in the system. This grooms the line items and appends them to the final invoice
        try:
            subscription = recurlyRateLimitWrapper(invoice.subscriptions())[0]
            for line_item in invoice.line_items:
                account_code = line_item.product_code[:30] if line_item.product_code != None else "None"
                line_item_type = returnIsSubscription(line_item.origin)
                subscription_external_id = subscription.uuid
                plan_code = returnPlanUUID(subscription.plan.plan_code)
                service_period_start = line_item.start_date
                service_period_end = line_item.end_date
                amount_in_cents = 0 if zeroInvoice else line_item.total_in_cents
                description = line_item.description
                discount_amount_in_cents = 0 if zeroInvoice else line_item.discount_in_cents
                tax_amount_in_cents = 0 if zeroInvoice else line_item.tax_in_cents
                account_code =  account_code# Takes the first 30 chars of the product code, CM only allows 30
                # Creates the final, groomed line item
                final_line_item = {

                    "type": line_item_type,
                    "subscription_external_id": subscription_external_id,
                    "plan_uuid": plan_code,
                    "service_period_start": service_period_start,
                    "service_period_end": service_period_end,
                    "amount_in_cents": amount_in_cents,
                    "discount_amount_in_cents": discount_amount_in_cents,
                    "tax_amount_in_cents": tax_amount_in_cents,
                    "description": description,
                    "account_code": account_code       
                }

                # Determines if another call needs to be made to retrieve the coupon code. Coupon codes do not show up on the dashboard, but are stored and can be retrieved by the API
                if(discount_amount_in_cents > 0):
                    discount_code = recurlyRateLimitWrapper(invoice.redemption()).coupon_code
                else:
                    discount_code = None


                # If there is a discount code, adds that to the line item
                if(discount_code): final_line_item['discount_code'] = discount_code
                    
        except Exception as ex:
            for line_item in invoice.line_items:
                account_code = line_item.product_code[:30] if line_item.product_code != None else "None"
                line_item_type = returnIsSubscription(line_item.origin)
                subscription_external_id = None
                plan_code = None
                service_period_start = line_item.start_date
                service_period_end = line_item.end_date
                amount_in_cents = 0 if zeroInvoice else line_item.total_in_cents
                description = line_item.description
                discount_amount_in_cents = 0 if zeroInvoice else line_item.discount_in_cents
                tax_amount_in_cents = 0 if zeroInvoice else line_item.tax_in_cents
                account_code =  account_code# Takes the first 30 chars of the product code, CM only allows 30
                # Creates the final, groomed line item
                final_line_item = {

                    "type": line_item_type,
                    "subscription_external_id": subscription_external_id,
                    "plan_uuid": plan_code,
                    "service_period_start": service_period_start,
                    "service_period_end": service_period_end,
                    "amount_in_cents": amount_in_cents,
                    "discount_amount_in_cents": discount_amount_in_cents,
                    "tax_amount_in_cents": tax_amount_in_cents,
                    "description": description,
                    "account_code": account_code       
                }

                # Determines if another call needs to be made to retrieve the coupon code. Coupon codes do not show up on the dashboard, but are stored and can be retrieved by the API
                if(discount_amount_in_cents > 0):
                    discount_code = invoice.redemption().coupon_code
                else:
                    discount_code = None


                # If there is a discount code, adds that to the line item
                if(discount_code): final_line_item['discount_code'] = discount_code
            
            # Adds the finished line item to the final invoice
        final_invoice["line_items"].append(final_line_item)

        # Grooms transactions and adds them to the final invoice
        for transaction in invoice.transactions:

            if(transaction.action =='purchase'): 
                type = 'payment'
            elif(transaction.action == 'verify'): # This represents an empty transaction to verify payment methods
                continue
            elif(transaction.action == "refund"):
                type = "refund"

            if(transaction.status == "success"):
                result = "successful"
            elif(transaction.status=="declined" or transaction.status == "void"):
                result = "failed"
                return # Ignore failed transactions

            final_transaction = {
                'date': transaction.created_at,
                'type': type,
                "result": result
            }
            final_invoice["transactions"].append(final_transaction)

        if(zeroInvoice):
            final_transaction = {
                'date': invoice.created_at,
                'type': "payment",
                "result": "successful"
            }
            final_invoice["transactions"].append(final_transaction)
            
        chartmogul.Invoice.create(config,uuid=cm_uuid, data={"invoices": [final_invoice]}).get()
       
        billing_info = account.billing_info
        
        if(billing_info.country =="US"):
            zipcode = zipcodes.matching(billing_info.zip)[0]
            chartmogul.Customer.modify(
            config,
            uuid=cm_uuid,
            data={      
                "city": zipcode['city'].title(),
                "country": zipcode['country'],
                "state": zipcode['state'],
                "address_zip": billing_info.zip
            })
        else:
            chartmogul.Customer.modify(
            config,
            uuid=cm_uuid,
            data={      
                "country": billing_info.country
            })

    except Exception as ex: 
        print(ex)
        # appendErrors("getRecurlyInvoicesByAPI", str(email), str(user_id), str(final_invoice),"None", str(external_id), str(ex), str(invoice), str(cm_uuid))

# Gathers recurly invoices and calls getRecurlyInvoicesByAPI
def runAddRecurly(start, end):    
    # Retrieves users recurly account and invoices
    invoices = []
    
    for invoice in recurly.Invoice.all(begin_time=start, end_time=end):
        invoices.append(invoice)
    
    runConcurrently(invoices, recurlyRateLimitWrapper(getRecurlyInvoicesByAPI))

#############################
# Cancel Newly Cancelled Subscriptions
#############################

# Chart Mogul requires that subscriptions be cancelled via another endpoint, and not by the invoice creator. 
# This function goes through the account, finds the cancelled subscriptions from recurly, and 
# then cancels the appropriate subscriptions in Chart Mogul
def cancelRecurlySubscriptions(subscription):
    try:
        account = recurlyRateLimitWrapper(subscription.account())
        account_code = account.account_code
        user = usersOnID.loc[usersOnID['recurly']== account_code]
        email = user['email']
        cm_uuid = getCMuuid(email)
        if(cm_uuid == None):
            cm_uuid = getCMuuid(account.email)
        if(cm_uuid == None):
            cm_uuid = getCMuuid(user['customer_email'])

        if(subscription.expires_at is not None): #If there is an expiration date, make that the cancellation date, otherwise use the canceled_at date. Chart Mogul processes churn based on the provided cancellation date, so the expiration date is more accurate if it's available. 
            canceled_at = subscription.expires_at
        else:
            canceled_at = subscription.canceled_at


            CM_subscriptions = chartmogul.Subscription.list_imported(config, uuid=cm_uuid).get().subscriptions
            for CM_subscription in CM_subscriptions:
                if(CM_subscription.external_id == subscription.uuid):
                    data={'cancellation_dates':[canceled_at]}
                    try:
                        chartmogul.Subscription.cancel(config, uuid=CM_subscription.uuid, data=data).get()
                        print(".",end="")
                    except Exception as ex: 
                        if('No cancelled_at value can be the same as any service_period_start value for a given subscription.' in str(ex)):
                            canceled_at = canceled_at + timedelta(minutes=1)
                            data={'cancelled_at':canceled_at}
                            chartmogul.Subscription.cancel(config, uuid=CM_subscription.uuid, data=data).get()
                            print(".",end="")
    except Exception as ex:
        print(ex)
        #  errors.append(("cancelRecurlySubscriptions", str(email), str(user['id']), "None", "None",str(account_code),str(ex),str(subscription),cm_uuid ))

# Cancels recurly subscriptions that have been cancelled or expired               
def runCancelRecurly(start, end):
    
    runConcurrently(recurlyRateLimitWrapper(recurly.Subscription.all(state="canceled", begin_time=start, end_time=end)), cancelRecurlySubscriptions,20)
    runConcurrently(recurlyRateLimitWrapper(recurly.Subscription.all(state="expired", begin_time=start, end_time=end)), cancelRecurlySubscriptions,20)

###############################
# Add New Stripe Orders
###############################
# Takes a Stripe order, grooms it, and then adds it to Chart Mogul
def addStripeOrder(order, type="payment"):
    try:
        
        cm_uuid = getCMuuid(order.email)
        
        if(cm_uuid == None):
            cm_uuid = createPlaceholderUser(order, "Stripe")
        final_invoice = {
            "external_id": order.id,
            "date": datetime.fromtimestamp(order.created),
            "currency": "USD",
            "line_items": [],
            "transactions": []
        }
        for item in order['items']:
            
            final_line_item = {
                    "type": "one_time",
                    "amount_in_cents": item.amount,
                    "description": item.description,
                    "account_code": item.parent    
                }
            final_invoice['line_items'].append(final_line_item)

        if(order.status == "paid" or order.status=="fulfilled" or order.status=="returned"):
            result = "successful"
        else: 
            result="failed"
            return #Ignore failed transactions
            
        try: 
            date = datetime.fromtimestamp(order.status_transitions.paid)
        except:
            date = datetime.fromtimestamp(order.created)
            
        final_transaction = {
                'date': date,
                'type': type,
                "result": result
            }
        final_invoice["transactions"].append(final_transaction)

        chartmogul.Invoice.create(config,uuid=cm_uuid, data={"invoices": [final_invoice]}).get()
        
        chartmogul.Customer.modify(
            config,
            uuid=cm_uuid,
            data={      
            "city": order.shipping.address.city,
            "country": order.shipping.address.country,
            "state": order.shipping.address.state,
            "address_zip": order.shipping.address.postal_code
            })
    except Exception as ex:
        print(ex)
       

# Retrieves and returns the Stripe orders for a given interval
def getStripeOrders(start, end):
    output = []

    orders = stripe.Order.list(created={'gte':start, 'lte':end })
    for order in orders.auto_paging_iter():
        output.append(order)
    return output

# Gathers Stripe refunds and adds them to CM
def getStripeRefunds(start, end):
    refunds = stripe.Refund.list(created={'gte':global_start_timestamp, 'lte':now_timestamp})
    for refund in refunds.auto_paging_iter():
        charge = stripe.Charge.retrieve(refund['charge'])
        order = stripe.Order.retrieve(charge['order'])
        cm_uuid = getCMuuid(order['email'])
        print(cm_uuid)

        final_invoice = {
            "external_id": refund.id,
            "date": datetime.fromtimestamp(refund.created),
            "currency": "USD",
            "line_items": [],
            "transactions": []
        }
        
        final_line_item = {
                "type": "one_time",
                "amount_in_cents": refund.amount,
                "description": "Refund for Order " + order.id,
                "account_code": "refund"       
            }
        final_invoice['line_items'].append(final_line_item)
        if(refund.status == "succeeded"):
            result = "successful"
        else: 
            result="failed"


        final_transaction = {
                'date': datetime.fromtimestamp(refund.created),
                'type': 'refund',
                "result": result
            }
        final_invoice["transactions"].append(final_transaction)
        
        try:
            print(chartmogul.Invoice.create(config,uuid=cm_uuid, data={"invoices": [final_invoice]}).get())
            print('*')
        except Exception as ex:
            if("{'plan_uuid': ['Field may not be null.']}" in str(ex)):
                return
            # appendErrors("getStripeRefunds", order['email'], None, str(final_invoice), refund['id'], None, str(ex), str(refund), cm_uuid)
            # print('X')

# Runs the Stripe order scripts
def runAddStripeOrders(start, end):
    runConcurrently(getStripeOrders(start, end), addStripeOrder)
    getStripeRefunds(start, end)

##########################
# Run Scripts
##########################

runAddUsers(newUsersOnEmail)
runAddRecurly(global_start_string,now_string)
runCancelRecurly(global_start_string,now_string)
runAddStripeOrders(global_start,now)

# writeErrors()
# pprint.pprint(errors)
